# README #

MiniPass is probably the lightest personal password manager in the world.

### PROs ###

* Extremely light-weight: just 3 plain text files.
* Absolute transparent: no obscure binaries, no unknown logics, all under your eyes and in your control.
* Secure: Stanford Javascript Crypto Library (http://crypto.stanford.edu/sjcl/)

### CONs ###

* No integrition with any other systems, so...no "1-click" filling convenience.
* If you don't want to go for the web hosting hassle, then it's only accessible from local disk files.
    * with a web hosting service, you can access your password anywhere in the world

### Setup ###

* Download the source individually, or from _download_ section and exract to anywhere you like
* Only 3 files needed though:
    * mp.html
    * mp.css
    * mp.js

### Run ###
* Open "mp.html" in a modern browser (chrome/firefox/opera/etc.)

### First Time Usage ###
* Use the initial pad interface to enter a salt word (click on each cells)
* Then click the blue title bar (cell with text: == KEY ==)
* Now you are in the inner session and feel free to
    * click "+" to create new domain/user/password entries
	* click "?" to do a parital matching search for your entries
	* click any other cells for indexed browsing of your entries
* Finally, please:
	* click "!" and then the upside down "delta" sign to download the encrypted "key.txt"
	* find this "key.txt" file in your download folder and move it to the same directory of the mp.html

### Normal Usage ###
* Next time you launch/reload the "mp.html", you will have to click the matching salt word to enter the inner session. (once it finds a "key.txt" file)

### Deploy to cloud ###

* Nothing prevents this app from running at a hosted web server. It does not require any fancy techniques.
* However, each time you make updates and download a new "key.txt", you will have to upload it to the hosted server again.

### How to make backups? ###

* It's all up to you. Easiest way might be using a version control tool, like Git or Svn...

### Outtro/About ###

* I'm a minimalism programmer, so as this minipass app.
* I've been using it as my own password solution for years, and never switched.
* If you see no activity in this project for a long time, it does NOT mean I give it up. Most likely I'm just pleased with it and lack of motivation to make any further changes...
